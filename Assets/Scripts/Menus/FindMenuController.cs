﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FindMenuController : MonoBehaviour, MenuController {
    // Reference buttons with these const strings
    const string BACK = "BACK";

	// TODO not use this shitty gui
	void OnGUI()
	{
		if (!Network.isClient && !Network.isServer)
		{
			// START SERVER
			/*if (GUI.Button(new Rect(100, 100, 250, 100), "Start Server"))
                StartServer();*/
			
			// REFRESH HOST LIST
			if (GUI.Button(new Rect(100, 250, 250, 100), "Refresh Hosts"))
				NetworkManager.RefreshHostList();
			
			// JOIN SERVER
			if (NetworkManager.hostList != null) {
				for (int i = 0; i < NetworkManager.hostList.Length; i++) {
					if (GUI.Button(new Rect(400, 100 + (i*110), 300, 100), NetworkManager.hostList[i].gameName)) {
						NetworkManager.JoinServer(NetworkManager.hostList[i]);
					}
				}
			}
		}
	}

    public void HandleButton (string btnName)
    {
        switch (btnName) {
            case BACK:
                btn_Back();
                break;
        }
    }

    public void btn_Back() {
        MainMenuController.RequestMenuChange(MenuState.MAIN);
    }
}
