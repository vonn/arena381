﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public enum MenuState { NONE, MAIN, FIND, HOST, LOBBY, ABOUT, QUIT }

public class MainMenuController : MonoBehaviour, MenuController
{
    #region STATIC VARS
    // Reference buttons with these const strings
    const string ABOUT = "ABOUT";
    const string QUIT = "QUIT";
    const string HOSTGAME = "HOSTGAME";
    const string FINDGAME = "FINDGAME";

    static MenuState _currState;
    public static MenuState CurrentMenuState
    {
        get { return _currState; }
        private set
        {
            PreviousMenuState = _currState;
            _currState = value;
        }
    }
    public static MenuState PreviousMenuState { get; private set; }

    static MainMenuController m_instance;
    public static MainMenuController M_Instance
    {
        get { return m_instance; }
        set { m_instance = value; }
    }
    #endregion

    #region Member vars
    Button btnHost, btnFind, btnAbout, btnQuit;

    [SerializeField]
    AudioClip btnMouseOverSFX, btnClickSFX;
    #endregion

    void Awake()
    {
        CurrentMenuState = MenuState.MAIN;

        if (m_instance == null)
        {
            print("Setting MMCtrl");
            m_instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (this != m_instance)
        {
            //print("Destroying MMCtrl");
            Destroy(this.gameObject);
            return;
        }

        SetupHandlers();
    }

    void OnLevelWasLoaded(int level)
    {
        if (Application.loadedLevelName == Scenes.MainMenu)
        {
            SetupHandlers();
        }
    }

    void SetupHandlers()
    {
        if (Application.loadedLevelName != Scenes.MainMenu) return;
        // retrieve the buttons
        btnHost = GameObject.Find("btnHost").GetComponent<Button>();
        btnFind = GameObject.Find("btnFind").GetComponent<Button>();
        btnAbout = GameObject.Find("btnAbout").GetComponent<Button>();
        btnQuit = GameObject.Find("btnQuit").GetComponent<Button>();

        // click listeners
        btnHost.onClick.AddListener(() =>
        {
            HandleButton(HOSTGAME);
        });
        btnFind.onClick.AddListener(() =>
        {
            HandleButton(FINDGAME);
        });
        btnAbout.onClick.AddListener(() =>
        {
            HandleButton(ABOUT);
        });
        btnQuit.onClick.AddListener(() =>
        {
            HandleButton(QUIT);
        });
    }

    public void HandleButton(string buttonName)
    {
        //PreviousMenuState = CurrentMenuState;

        switch (buttonName)
        {
            case ABOUT:
                print("bonk");
                CurrentMenuState = MenuState.ABOUT;
                Application.LoadLevel(Scenes.AboutMenu);
                break;
            case QUIT:
                print("moop");
                CurrentMenuState = MenuState.QUIT;
                Application.Quit();
                break;
            case HOSTGAME:
                RequestMenuChange(MenuState.HOST);
                break;
            case FINDGAME:
                RequestMenuChange(MenuState.FIND);
                break;
            default:
                throw new UnityException("UNKNOWN BUTTON PRESS");
        }
    }

    /// <summary>
    /// Handles changing of the scene.
    /// </summary>
    /// <returns><c>true</c>, if menu change was requested, <c>false</c> otherwise.</returns>
    /// <param name="newState">New state.</param>
    public static void RequestMenuChange(MenuState newState)
    {
        switch (newState)
        {
            case MenuState.MAIN:
                CurrentMenuState = MenuState.MAIN;
                Application.LoadLevel(Scenes.MainMenu);
                break;
            case MenuState.HOST:
                CurrentMenuState = MenuState.HOST;
                Application.LoadLevel(Scenes.HostGame);
                break;
            case MenuState.FIND:
                CurrentMenuState = MenuState.FIND;
                Application.LoadLevel(Scenes.FindGame);
                break;
            case MenuState.LOBBY:
                CurrentMenuState = MenuState.LOBBY;
                Application.LoadLevel(Scenes.Lobby);
                break;
        }
    }

    // called when i'm going into game
    public static void KillMainMenu()
    {
        Destroy(m_instance);
    }

    #region SFX // HACKY WAY TO PLAY BUTTON SOUNDS
    public void btnMouseOver()
    {
        GameObject.FindGameObjectWithTag(Tags.MenuController).audio.PlayOneShot(btnMouseOverSFX);
    }

    public void btnClick()
    {
        GameObject.FindGameObjectWithTag(Tags.MenuController).audio.PlayOneShot(btnClickSFX);
    }
    #endregion
}
