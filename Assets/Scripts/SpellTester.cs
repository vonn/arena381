﻿using UnityEngine;
using System.Collections;
// Stick this on a spell prefab and hit f3 to shoost it
public class SpellTester : MonoBehaviour {

	public Spell spell;
	public PvPAvatar owner;
	public KeyCode keyBind;
	
	/*void Awake() {
		spell = GetComponent<Spell>();
	}*/
	
	void Start() {
		print ("SPELL TESTER PRIMED: PRESS " + keyBind + " TO ATTACK PLAYER 2 WITH A SPELL");
	}

	void Update () {
		if (Input.GetKeyDown(keyBind)) {
			if (spell.Caster.canCast) {
				spell.CastTargeted(spell.Caster.Caster, spell.Target.Target);
				print ("firing test spell, " + transform.name);
			}
			else {
				print ("Cannot cast!");
			}
		}
	}
}
