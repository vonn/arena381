﻿using UnityEngine;
using System.Collections;

public class TeamController : MonoBehaviour {
	[SerializeField] public Material[] teamMaterials = new Material[2];
	
	public static void SetTeam(int teamNum, PvPAvatar pvpAv) {
		pvpAv.networkView.RPC ("SetTeam", RPCMode.AllBuffered, teamNum);
	}
}
