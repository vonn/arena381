﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Helper things related to Layers
/// </summary>
public class Layers
{
    // Layers and their indices, as defined in Project Settings -> Tags & Layers
    public const int defaultLayer = 0;
    public const int transparentFXLayer = 1;
    public const int ignoreRaycastLayer = 2;
    public const int waterLayer = 4;
    public const int uiLayer = 5;
    public const int terrainLayer = 8;
    public const int pvpAvatarLayer = 9;

    // Masks
    public const int navGroundMask = 1 << terrainLayer;
    public const int pvpAvMask = 1 << pvpAvatarLayer;
}

public class Scenes
{
    public const string MainMenu = "00_MainMenu";
    public const string AboutMenu = "10b_AboutMenu";
    public const string HostGame = "20c_HostGame";
    public const string FindGame = "30d_FindGame";
    public const string Lobby = "40e_Lobby";

    public const string Test1 = "Test";
    public const string Arena = "_FinalArena";
}

public class Tags
{
    public const string MenuController = "MenuController";
}

/// <summary>
/// A reference list of all the spell names
/// </summary>
public class Spells
{
    public const string BasicAttack = "BasicAttack";
    public const string Fireball = "Fireball";
    public const string Heal = "Heal";
    public const string Tower_Attack = "Tower_Attack";
}

public enum DamageType { PHYSICAL, SPELL, TRUE, NONE };