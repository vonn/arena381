using UnityEngine;
using System.Collections;

/// <summary>
/// A definition for a hero
/// </summary>
[System.Serializable]
public class Hero {
    static int MAX_SPELLS = 3;

    [SerializeField]
    protected string[] heroSpells = new string[MAX_SPELLS];

    [SerializeField]
    protected string heroName;

}