#define DEBUG
using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

/// <summary>
/// Avatar state.
/// </summary>
public enum AvatarState { IDLE, ATTACKING, CASTING, DEAD };
/// <summary>
/// Which team an avatar belongs to
/// </summary>
public enum Team { ONE, TWO, NEUTRAL };
/// <summary>
/// Avatar type.
/// </summary>
public enum AvatarType { PLAYER, MINION, BUILDING, TURRET };

/// <summary>
/// A class that defines an avatar that can conduct pvp combat. That is,
/// minions, players, etc.
/// </summary>
[RequireComponent(typeof(NavMeshAgent))]
public class PvPAvatar : MonoBehaviour
{
    #region STATIC FIELDS
    public static int PvPAvatarID = 0;
    #endregion

    #region MEMBER FIELDS
    public int ID = 0;
    public string character; //hero name or minion type or building type
    public GameObject _teamOne, _teamTwo;
    public NavMeshAgent _navAgent;
    public Transform _trans;
    protected int _floorMask;

    [SerializeField]
    public Team _team;
    [SerializeField]
    public AvatarState _state = AvatarState.IDLE;
    [SerializeField]
    protected AvatarType _type;
    public AvatarType Type
    {
        get { return _type; }
    }

    [SerializeField]
    public PvPAttributes _attr;

    public GameObject healthBar = null;

    /// <summary>
    /// Attributes (stats, values, etc) of the PvPAv.
    /// </summary>
    /// <value>The attributes.</value>
    public PvPAttributes Attributes
    {
        get { return _attr; }
    }

    [SerializeField]
    protected float aggroRadius = 1.0f; // how close before i bite!
    protected Vector3 currentDestination; // where am i currently going?
    [SerializeField]
    protected bool lookForFight; // is my current move command an attack move?
    [SerializeField]
    protected bool stopped; // prevents from autoattacking
    [SerializeField]
    protected bool inCombat;
    [SerializeField]
    protected bool isPlayer;
    [SerializeField]
    protected PvPAvatar currentTarget;
    bool isMoving;
    protected bool pursueTarget;

    protected float nextAtkTime; // when am i attacking next?
    const float chaseCD = .75f;
    float nextChaseTime;
    const float aggroCheckCD = .6f;
    float nextAggroCheckTime;

    public bool isPassive;

    // My Spells
    [SerializeField]
    ArrayList spells = new ArrayList();
    #endregion

    #region Static methods
    public static PvPAvatar GetByID(int id)
    {
        if (id < 0) return null;

        // for all the PvPAvatars
        PvPAvatar[] avs = FindObjectsOfType<PvPAvatar>();
        PvPAvatar result = null;

        for (int i = 0; i < avs.Length && result == null; i++)
        {
            if (avs[i].ID == id)
            {
                result = avs[i];
            }
        }

        return result;
    }
    #endregion

    #region UNITY METHODS
    protected virtual void Awake()
    {
        if (Network.isServer)
        {
            ID = PvPAvatar.PvPAvatarID;
            PvPAvatar.PvPAvatarID += 1;
            //print("PONK FROM " + name + ": #" + ID);
        }

        _trans = transform;
        _floorMask = Layers.navGroundMask;
        _navAgent = GetComponent<NavMeshAgent>();

        _teamOne = GameObject.Find("Team 1");
        _teamTwo = GameObject.Find("Team 2");

        // set the speed of my navmeshagent to my run speed
        _navAgent.speed *= _attr.runSpd;

        // Set up default spell -- autoattack
        spells.Add(Spells.BasicAttack);

        // give me a healthbar!
        healthBar = GameController.instance.ui.CreateHealthBar(this);
    }

    protected virtual void FixedUpdate()
    {
        // am I dead? || Also, don't let clients update themselves while chasing (let server handle)
        if (_state == AvatarState.DEAD || !networkView.isMine) 
		{
			return;
		}

        // Drop dead targets
        if ((currentTarget != null && currentTarget._state == AvatarState.DEAD) ||     // 1 - target is dead, or
            (currentTarget != null && !pursueTarget && DistToTarget() > aggroRadius))  // 2 - target is too far
        {
            DropTarget(true);
            networkView.RPC("DropTarget", RPCMode.Others, new object[] { true });
            lookForFight = true;
        }

        // I has target?
        if (currentTarget != null)
        {
            // target is enemy, and i'm attacking!
            if (IsEnemy(currentTarget) && _state == AvatarState.ATTACKING)
            {
                // Players and minions chase targets
                if (_type != AvatarType.TURRET)
                {
                    // attack if i'm close enough
                    if (IsTargetInAtkRange()) {
                        // make sure i'm stopped
                        _navAgent.Stop();

                        // Time to strike?
                        if (Time.time >= nextAtkTime) {
                            AutoAttack();
                            //networkView.RPC("AutoAttack", RPCMode.Others, null);
                        }
                    }
                    // chase target
                    else
                    {
                        _navAgent.Resume();
                        //nextAtkTime = Time.time + _attr.atkSpd;

                        if (Time.time >= nextChaseTime)
                        {
                            nextChaseTime = Time.time + chaseCD;

                            ChaseTarget();
                            //networkView.RPC("ChaseTarget", RPCMode.Others, null);
                        }
                    }
                }
            }
        }
        // no target
        else
        {
            // am i stuck attacking? fix that.
            if (_state == AvatarState.ATTACKING) 
                _state = AvatarState.IDLE;
        }
    }

    protected virtual void LateUpdate()
    {
        // again, don't let clients update
        if (Network.isClient) return;

        if (_state == AvatarState.DEAD) return;

        // have i reached my current destination?
        if (isMoving && Vector3.Distance(currentDestination, _trans.position) <= .005f)
        {
            //StopAndLookForFight();
            networkView.RPC("StopAndLookForFight", RPCMode.All, null);
        }

        if (currentTarget == null && lookForFight && Time.time >= nextAggroCheckTime)
        {
            AggroCheck();
            //networkView.RPC("AggroCheck", RPCMode.Others, null);
        }
    }
    #endregion

    #region RPC Mutators
    [RPC]
    public void SetID(int newID)
    {
        ID = newID;
    }
    #endregion

    #region Target Handling
    /// <summary>
    // cast a line toward my target, and return true if it collides with target's surface
    /// </summary>
    /// <returns></returns>
    public bool IsTargetInAtkRange()
    {
        if (currentTarget == null) return false;

        // vec to my target
        Vector3 dir = (currentTarget._trans.position - _trans.position).normalized;

        Ray ray = new Ray(_trans.position, dir);

        // cast ray to target
        RaycastHit[] hitInfos = Physics.RaycastAll(ray, _attr.atkRange, Layers.pvpAvMask);

        bool targetHit = false;
        // see if target was hit
        for (int i = 0; i < hitInfos.Length && !targetHit; i++)
        {
            // get the PvPAv component of this hit, is it my target?
            if (hitInfos[i].transform.GetComponent<PvPAvatar>().Equals(currentTarget))
            {
                targetHit = true;
            }
        }

    #if DEBUG
        Color col = targetHit ? Color.green : Color.red;
        Debug.DrawRay(ray.origin, ray.direction, col);
    #endif

        return targetHit;
    }

    /// <summary>
    /// Stops moving and looks for a fight~~
    /// </summary>
    [RPC]
    public void StopAndLookForFight()
    {
        _navAgent.Stop();
        isMoving = false;
        //print ("reached destination");
        lookForFight = true;
    }

    /// <summary>
    /// Is this av my enemy?
    /// </summary>
    /// <param name="av"></param>
    /// <returns></returns>
    public bool IsEnemy(PvPAvatar av)
    {
        return _team != av._team;
    }

    /// <summary>
    /// Chase my target
    /// </summary>
    [RPC]
    public void ChaseTarget()
    {
        SetDestination(currentTarget._trans.position);
    }

    /// <summary>
    /// Sets up a target to attack
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    public void SetTarget(PvPAvatar avatar, bool pursue = false)
    {
        if (avatar == null) return;

        lookForFight = false || _type == AvatarType.TURRET;
        currentTarget = avatar;
        pursueTarget = pursue; // SHOULD I STICK TO HIM EVEN IF HE'S OUT OF RANGE?

        // HOSTILE TARGET
        if (avatar._team != _team)
        {
            _state = AvatarState.ATTACKING;
        }
        // FRIENDLY TARGET
        else
        {
            _state = AvatarState.IDLE;
            SetDestination(avatar._trans.position);
            //networkView.RPC("SetDestination", RPCMode.Others, new object[] { avatar._trans.position });
        }
    }

    /// <summary>
    /// RPC version of settarget using a LOOKUP TABLE
    /// </summary>
    /// <param name="avatarID"></param>
    [RPC]
    public void SetTargetRPC(int avatarID, bool pursue)
    {
        PvPAvatar tarAv = PvPAvatar.GetByID(avatarID);
        SetTarget(tarAv, pursue);
    }

    /// <summary>
    /// Drop the current target
    /// </summary>
    [RPC]
    public void DropTarget(bool hostileAfter)
    {
        currentTarget = null;
        pursueTarget = false;
        lookForFight = hostileAfter;
        _state = _state == AvatarState.DEAD ? AvatarState.DEAD : AvatarState.IDLE;
    }

    /// <summary>
    /// Determine world distance to my target
    /// </summary>
    /// <returns></returns>
    protected float DistToTarget()
    {
        if (currentTarget == null) return -1;

        return Vector3.Distance(_trans.position, currentTarget._trans.position);
    }

    /// <summary>
    /// Check within aggro radius for possible targets, and set the target is there is a valid one available
    /// </summary>
    [RPC]
    protected bool AggroCheck()
    {
        Collider[] results = Physics.OverlapSphere(_trans.position, aggroRadius, Layers.pvpAvMask);
        bool anyHit = results.Length > 0;
        if (!anyHit) return false;

        Collider nearestCol = results[0];
        float nearestDistance = float.MaxValue;
        float distance;

        for (int i = 0; i < results.Length; i++)
        {
            Collider c = results[i];
            // ignore myself
            if (c.transform == _trans) continue;

            // is it the nearest one?
            distance = (_trans.position - c.transform.position).sqrMagnitude;
            if (distance < nearestDistance)
            {
                nearestDistance = distance;
                nearestCol = c;
            }
            else continue;

            PvPAvatar av = c.GetComponent<PvPAvatar>();
            //print(av.name);

            // make sure they are on different team and alive
            if (av._team != _team && av._state != AvatarState.DEAD)
            {
                // only hit neutrals if they are in combat
                if (av._team == Team.NEUTRAL && !av.inCombat) 
                    continue;

                //print(_trans.name + " can attack " + av.name + " from " + av._team + " team.");

                SetTarget(av);
                networkView.RPC("SetTargetRPC", RPCMode.Others, new object[] { av.ID, false });
            }
        }

        // set next check time
        nextAggroCheckTime = Time.time + aggroCheckCD;

        return anyHit;
    }
    #endregion

    #region Damage Handling
    /// <summary>
    /// Attacks the current target, if the time is right
    /// </summary>
    [RPC]
    protected void AutoAttack()
    {
        // deal physical dmg equal to my atk power
        GiveDamage(currentTarget, _attr.atkPwr, DamageType.PHYSICAL);

        // update next atk time
        nextAtkTime = Time.time + _attr.atkSpd;
    }

    /// <summary>
    /// Takes the damage from a Spell obj
    /// </summary>
    /// <param name="amount">Amount.</param>
    /// TODO redo this as RPC-able method
    /// 

    // DONT USE THIS
    /*public bool TakeDamageSpell(int amount, Spell source = null)
    {
        // Take damage
        Attributes.currHealth -= amount;

        // WHERE DID I TAKE DMG FROM, FUCK.
        if (source != null)
        {
            print(transform.name + " took " + amount + " damage from " + source.Caster.Caster.transform.name + "'s " + source.SpellName);
        }

        // DID I DIE ?
        if (Attributes.currHealth <= 0)
        {
            Attributes.currHealth = 0;

            //If I was killed by a player give them my value in xp
            if (source.Caster.Caster._type == AvatarType.PLAYER)
            {
                source.Caster.Caster.GainExp(_attr.expValue);
            }

            Die();
        }

        return true;
    }*/

    /// <summary>
    /// Take a flat amount of dmg
    /// </summary>
    /// <param name="dmgAmount"></param>
    /// <param name="dmgType"></param>
    public void TakeDamage(int dmgAmount, int dmgType, int srcAvID)
    {
        // TODO armor/spell pen not considered
        // Mitigate based on damage type
        switch ((DamageType)dmgType)
        {
            // mitigate with armor
            case DamageType.PHYSICAL:
                // i.e. 10 armor should reduce dmg by 10%
                dmgAmount *= (1 - _attr.armor / 100);
                break;

            // mitigate with spell resist
            case DamageType.SPELL:
                // i.e. 10 spell resist should reduce dmg by 10%
                dmgAmount *= (1 - _attr.spellResist / 100);
                break;

            // True dmg cannot be mitigated
            case DamageType.TRUE:
                break;

            // this shouldn't happen
            case DamageType.NONE:
                dmgAmount = 0;
                break;
        }

        // let everyone know my health changed
        //networkView.RPC("ModifyHealth", RPCMode.All, new object[] { -dmgAmount, srcAvID });
        ModifyHealth(-dmgAmount, srcAvID);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="delta"></param>
    public void ModifyHealth(int delta, int srcAvID)
    {
        int newHP = _attr.currHealth;
        newHP += delta;

        // did i died
        if (newHP <= 0)
        {
            newHP = 0;
        }
        // is it overheal? set to max hp
        else if (newHP >= _attr.health)
        {
            newHP = _attr.health;
        }

        // tell everyone my updated HP
        SetHealth(newHP, srcAvID);
        networkView.RPC("SetHealth", RPCMode.Others, new object[] { newHP, srcAvID });
    }

    [RPC]
    public void SetHealth(int hp, int srcAvID = -1)
    {
        _attr.currHealth = hp;

        if (hp == 0)
        {
            // tell everyone i died
            // TODO real value, instead of -1
            if (_type == AvatarType.PLAYER)
            {
                // HACK? a little.
                GetComponent<PlayerController>().Die(srcAvID);
            }
            else
            {
                Die(srcAvID);
            }
            //networkView.RPC("Die", RPCMode.Others, new object[] { -1 });
        }
    }

    /// <summary>
    /// Give the target PvPAv damage
    /// </summary>
    /// <param name="target"></param>
    /// <param name="amount"></param>
    public void GiveDamage(PvPAvatar target, int amount, DamageType type)
    {
        // don't damage dead targets pls
        if (target._state == AvatarState.DEAD) return;

        // call TakeDamage(amount) on the target for everyone connected
        //target.TakeDamage(amount);
        //target.networkView.RPC("TakeDamage", RPCMode.All, new object[] { amount, (int)type, passID });
        target.TakeDamage(amount, (int)type, ID);
    }
    #endregion

    #region EXP
    /// <summary>
    /// Increase my EXP
    /// </summary>
    /// <param name="amount"></param>
    public void GainExp(int amount, bool rpcAfter = false)
    {
        int newEXP = _attr.exp;
        newEXP += amount;

        // is this enough to level me?
        if (newEXP >= _attr.expToLevel)
        {
            int excessEXP = newEXP - _attr.expToLevel;

            if (rpcAfter)
                networkView.RPC("LevelUp", RPCMode.All, null);
            else
                LevelUp(excessEXP);
        }
        else
        {
            // just update my exp
            if (rpcAfter)
                networkView.RPC("SetEXP", RPCMode.All, new object[] { newEXP });
            else
                SetEXP(newEXP);
        }

        print(name + " gains " + amount + " XP. Level " + _attr.level + ", " + _attr.exp + "/" + _attr.expToLevel);
    }

    [RPC]
    public void SetEXP(int amount)
    {
        _attr.exp = amount;
    }

    /// <summary>
    /// Levels up.
    /// xp to next level is 100 * level
    /// </summary>
    /// <param name="extraExp">Extra exp.</param>
    [RPC]
    public void LevelUp(int extraExp)
    {
        _attr.level++;
        // it takes me more exp to level up now
        _attr.expToLevel = _attr.level * 100;
        // i also give more exp (1.75x more)
        _attr.expValue = (int)(_attr.expValue * 1.5f);
        // set my exp (last recursive call will prevail)
        _attr.exp = extraExp;

        // increase my attributes
        float factor1 = 1.45f; // sure why not
        // health
        _attr.health = (int)(_attr.health * factor1);
        _attr.currHealth = _attr.health; // full heal
        // mana
        _attr.mana = (int)(_attr.mana * factor1);
        _attr.currMana = _attr.mana; // full mana

        float factor2 = 1.25f; // yeah sure
        // armor
        _attr.armor = (int)(_attr.armor * factor2);
        // spell resist
        _attr.spellResist = (int)(_attr.spellResist * factor2);
        // atk pow
        _attr.atkPwr = (int)(_attr.atkPwr * factor2);
        // atk speed
        _attr.atkSpd = _attr.atkSpd / factor2;
        // spell
        _attr.spellPwr = (int)(_attr.atkPwr * factor2 * factor2);

        // whoa, recursive leveling?
        if (extraExp >= _attr.expToLevel)
        {
            int excess = extraExp - _attr.expToLevel;

            LevelUp(excess);
        }
    }
    #endregion

    #region Life & Death
    /// <summary>
    /// Do the thing when you die
    /// </summary>
    /// <param name="killerAvID"></param>
    [RPC]
    public virtual void Die(int killerAvID)
    {
        PvPAvatar killer = PvPAvatar.GetByID(killerAvID);
        if (killer != null)
        {
            print(_trans.name + " HAS BEEN SLAIN BY " + killer.name);
        }
        else
        {
            print("ERROR: CAN'T DETERMINE KILLER");
        }

        // I'M DEAD MEOW
        _state = AvatarState.DEAD;
        _navAgent.Stop();

        int expRange = 7;

        // If Team One
        if (_team == Team.ONE)
        {
            //for all things on the enemy team
            foreach (PvPAvatar av in _teamTwo.GetComponentsInChildren<PvPAvatar>())
            {
                //That are players
                if (av._type == AvatarType.PLAYER)
                {
                    //If they are within xp range
                    if (Vector3.Distance(av._trans.position, _trans.position) <= expRange)
                    {
                        //Gain my xp value
                        av.GainExp(_attr.expValue);

                        //av.networkView.RPC("GainExp", RPCMode.Others, new object[] { _attr.expValue, false });
                    }
                }
            }
        }
        //If Team Two
        else if (_team == Team.TWO)
        {
            //for all things on the enemy team
            foreach (PvPAvatar av in _teamOne.GetComponentsInChildren<PvPAvatar>())
            {
                //That are players
                if (av._type == AvatarType.PLAYER)
                {
                    //If they are within xp range
                    if (Vector3.Distance(av._trans.position, _trans.position) <= expRange)
                    {
                        //Gain my xp value
                        av.GainExp(_attr.expValue);

                        //av.networkView.RPC("GainExp", RPCMode.Others, new object[] { _attr.expValue, false });
                    }
                }
            }
        }

        //After xp calculations, move the transform very far away
        //_navAgent.enabled = false;

        // NOTE: this stuff should probably be moved to the different scripts' Die() methods
        if (_type == AvatarType.MINION)
        {
            //ObjectPool.Recycle(this.gameObject);
            // obliterate over network
            networkView.RPC("DestroyHealthBar", RPCMode.All, null);
            Network.Destroy(gameObject);
        }
        //else if (_type == AvatarType.PLAYER)
        //{
        //    //set respawn timer HACKEY!!!
        //    //((PlayerController)this).respawnTime = Time.time + ((PlayerController)this).respawnCD;
        //}
        // this wasn't working first try so HACK into here w/e
        else if (_type == AvatarType.BUILDING || _type == AvatarType.TURRET)
        {
            ////End the game
            //if (_team == Team.ONE)
            //    GameController.instance.GameOver((int)Team.ONE);
            //else if (_team == Team.TWO)
            //    GameController.instance.GameOver((int)Team.TWO);
            //else
            //    GameController.instance.GameOver((int)Team.NEUTRAL);

            // just grey me out
            networkView.RPC("DestroyHealthBar", RPCMode.All, null);
            _trans.renderer.material.SetColor("_Color", Color.grey);

            // more hax idc
            if (GetComponent<MinionSpawner>() != null)
            {
                print("MAIN STRUCTURE DESTROYED");

                // EXCEPT WHEN I DIE, THE OTHER TEAM WINS THE GAME!
                int winningTeam = _team == Team.ONE ? (int)Team.TWO : (int)Team.ONE;
                GameController.instance.GameOver(winningTeam);
            }
        }
    }

    [RPC]
    public void DestroyHealthBar()
    {
        Destroy(healthBar.gameObject);
    }

    /// <summary>
    /// Respawn me
    /// </summary>
    [RPC]
    public virtual void Respawn()
    {
        if (_state != AvatarState.DEAD) return;

        // TODO redo this garbage
		Transform[] teamObjects;
		if(_team == Team.ONE)
		{
			teamObjects = _teamOne.GetComponentsInChildren<Transform>();
		}
		else if(_team == Team.TWO)
		{
			teamObjects = _teamTwo.GetComponentsInChildren<Transform>();
		}
		else teamObjects = null;
		foreach(Transform t in teamObjects)
		{
			if(t.CompareTag("Respawn"))
                // move to respawn position
				_trans.position = t.position;
		}

        // reset health and mana
        _attr.currHealth = _attr.health;
        _attr.currMana = _attr.mana;

        // i live
        _state = AvatarState.IDLE;

        // reenable my navmesh
        _navAgent.enabled = true;
    }
    #endregion

    #region Spells & Casting
    [RPC]
    protected void CastSpell(string spellName)
    {
        if (spells.Contains(spellName))
        {
            // TODO look in spells list for spell
            // cast the spell
        }
        else
        {
            print("ERROR: TRYING TO CAST A SPELL I DON'T OWN");
        }
    }
    #endregion

    /// <summary>
    /// Wrapper func to set the nav's destination.
    /// </summary>
    /// <param name="dest">Destination.</param>
    [RPC]
    public void SetDestination(Vector3 newDest)
    {
        newDest.y = _trans.position.y;
        isMoving = true;
        currentDestination = newDest;
        _navAgent.SetDestination(currentDestination);
    }

    [RPC]
    public void SetTeam(int teamNum)
    {
        renderer.material = GameObject.Find("TeamController").GetComponent<TeamController>().teamMaterials[teamNum];
        //print("Setting " + transform.name + "'s team to " + (Team)teamNum);
        _team = (Team)teamNum;
    }

    #region Network Sync
    /// <summary>
    /// WHAT GETS SERIALIZED AND SYNCHRONIZED
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="info"></param>
    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        if (stream.isWriting)
        {
            // let server do everything
            if (!networkView.isMine) return;

            // write my ID
            int id = ID;
            stream.Serialize(ref id);

            // and my position, sure
            Vector3 pos = _trans.position;
            stream.Serialize(ref pos);

            // my health
            stream.Serialize(ref _attr.health);
            // my mana
            stream.Serialize(ref _attr.mana);
            // exp
            stream.Serialize(ref _attr.exp);

            // state
            int stateID = (int)_state;
            stream.Serialize(ref stateID);

            // and maybe more
        }
        else
        {
            // read ID
            int id = -1;
            stream.Serialize(ref id);
            ID = id;

            // read position
            Vector3 pos = Vector3.zero;
            stream.Serialize(ref pos);
            // and apply it
            _trans.position = pos;

            // my health
            stream.Serialize(ref _attr.health);
            // my mana
            stream.Serialize(ref _attr.mana);
            // exp
            stream.Serialize(ref _attr.exp);

            // state
            int stateID = -1;
            stream.Serialize(ref stateID);
            _state = (AvatarState)stateID;

            // etc.
        }
    }
    #endregion

#if DEBUG
    void OnDrawGizmos()
    {
        if (isPassive) return;

        // Draw aggro sphere
        if (currentTarget == null)
        {
            Gizmos.color = lookForFight ? Color.red : Color.blue;
            Gizmos.DrawWireSphere(_trans.position, aggroRadius);
        }
        // draw line to target
        else
        {
            Gizmos.color = currentTarget._team == _team ? Color.green : Color.red;
            Gizmos.DrawLine(_trans.position, currentTarget._trans.position);
        }

        // draw attack range sphere
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(_trans.position, _attr.atkRange);

        // draw a lil line pointing forward by 2 unit
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(_trans.position, _trans.position + _trans.forward * 2f);
    }
#endif
}
