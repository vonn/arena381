﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	PlayerController _owner;
	public PlayerController Owner {
		get { return _owner; }
		set {
			_owner = value;
			SetupToPlayer();
		}
	}
	public PvPAvatar targetAvatar;

	public Text 	playerNameDisplay;
	public Text 	playerHealthDisplay;
	public Slider 	playerHealthSlider;
	public Text 	playerManaDisplay;
	public Slider 	playerManaSlider;
	public Slider 	playerExperienceSlider;
	public Text		playerAtkPwrDisplay;
	public Text		playerAtkSpdDisplay;
	public Text		playerSpellPwrDisplay;
	public Text		playerArmorDisplay;
	public Text		playerMRDisplay;
	public Text		playerLevelDisplay;

    public GameObject targetPanel; // folder for targetPanel stuff
	public Text 	targetNameDisplay;
	public Text 	targetHealthDisplay;
	public Slider 	targetHealthSlider;
	public Text 	targetManaDisplay;
	public Slider 	targetManaSlider;
	public Text		targetAtkPwrDisplay;
	public Text		targetAtkSpdDisplay;
	public Text		targetSpellPwrDisplay;
	public Text		targetArmorDisplay;
	public Text		targetMRDisplay;
	public Text		targetLevelDisplay;

    public GameObject deadPanel; // folder for things to show when dead (res timer, overlay)
    public Text resTimerDisplay;

	public Slider healthBarPrefab;
	public GameObject healthBarFolder;

	public Camera camera;
	public Canvas canvas;

	private ArrayList healthTuples = new ArrayList();

    AvatarState ownerStateLastFrame;

	void Awake()
	{
		//healthTuples = new Hashtable();

		if (Owner == null) return;
		SetupToPlayer();
	}

	void SetupToPlayer() {
		playerNameDisplay.text = Owner.character;
	}

	void Update()
	{
        // if my target just died
        if (ownerStateLastFrame != AvatarState.DEAD &&
            _owner._state == AvatarState.DEAD)
        {
            // show dead panel
            deadPanel.SetActive(true);
        }
        // if my target just res'd
        if (ownerStateLastFrame == AvatarState.DEAD &&
            _owner._state != AvatarState.DEAD)
        {
            // hide dead panel
            deadPanel.SetActive(false);
        }

        if (_owner._state == AvatarState.DEAD)
        {
            // update res timer
            float resTime = _owner.respawnTime;
            float timeLeft = resTime - Time.time;
            resTimerDisplay.text = timeLeft + "";
        }

		//Update Player UI
		UpdatePlayerHUD();

		//Update Target UI
		UpdateTargetHUD();

		//Update Health Bars
		UpdateHealthBars();

        // update the lastframe business
        ownerStateLastFrame = _owner._state;
	}

	void UpdateTargetHUD() {
		if(targetAvatar != null)
		{
            if (!targetPanel.activeSelf)
                targetPanel.gameObject.SetActive(true);

            targetNameDisplay.text = "" + targetAvatar.character;
			targetHealthDisplay.text = "" + targetAvatar.Attributes.currHealth + "/" + targetAvatar.Attributes.health;
			targetHealthSlider.maxValue = targetAvatar.Attributes.health;
			targetHealthSlider.value = targetAvatar.Attributes.currHealth;
			targetManaDisplay.text = "" + targetAvatar.Attributes.currMana + "/" + targetAvatar.Attributes.mana;
			targetManaSlider.maxValue = targetAvatar.Attributes.mana;
			targetManaSlider.value = targetAvatar.Attributes.currMana;
			targetAtkPwrDisplay.text = "AtkPwr " + targetAvatar.Attributes.atkPwr;
			targetAtkSpdDisplay.text = "AtkSpd " + targetAvatar.Attributes.atkSpd;
			targetSpellPwrDisplay.text = "SplPwr " + targetAvatar.Attributes.spellPwr;
			targetArmorDisplay.text = "Arm " + targetAvatar.Attributes.armor;
			targetMRDisplay.text = "MR " + targetAvatar.Attributes.spellResist;
			targetLevelDisplay.text = "" + targetAvatar.Attributes.level;
		}
        else
        {
            targetPanel.gameObject.SetActive(false);
        }
	}

	void UpdateHealthBars() {
        foreach (HealthTuple tuple in healthTuples)
        {
            if (tuple.avatar == null)
            {
                Destroy(tuple.slider);
                continue;
            }
            else if (tuple.slider != null)
            {
			    tuple.slider.transform.position = camera.WorldToScreenPoint(tuple.avatar.transform.position) + new Vector3(0f,40f,0f);
			    tuple.slider.maxValue = tuple.avatar.Attributes.health;
			    tuple.slider.value = tuple.avatar.Attributes.currHealth;
            }
        }
	}

	void UpdatePlayerHUD() {
		if (Owner == null) return;
		playerHealthDisplay.text = "" + Owner.Attributes.currHealth + "/" + Owner.Attributes.health;
		playerHealthSlider.maxValue = Owner.Attributes.health;
		playerHealthSlider.value = Owner.Attributes.currHealth;
		playerManaDisplay.text = "" + Owner.Attributes.currMana + "/" + Owner.Attributes.mana;
		playerManaSlider.maxValue = Owner.Attributes.mana;
		playerManaSlider.value = Owner.Attributes.currMana;
		playerExperienceSlider.maxValue = Owner.Attributes.exp + Owner.Attributes.expToLevel;
		playerExperienceSlider.value = Owner.Attributes.exp;
		playerAtkPwrDisplay.text = "AtkPwr " + Owner.Attributes.atkPwr;
		playerAtkSpdDisplay.text = "AtkSpd " + Owner.Attributes.atkSpd;
		playerSpellPwrDisplay.text = "SplPwr " + Owner.Attributes.spellPwr;
		playerArmorDisplay.text = "Arm " + Owner.Attributes.armor;
		playerMRDisplay.text = "MR " + Owner.Attributes.spellResist;
		playerLevelDisplay.text = "" + Owner.Attributes.level;
	}

	public GameObject CreateHealthBar(PvPAvatar owner)
	{
		HealthTuple newHealth = new HealthTuple();
        newHealth.slider = (Slider)Instantiate(healthBarPrefab);
		newHealth.slider.transform.SetParent(healthBarFolder.transform);
		newHealth.avatar = owner;
		healthTuples.Add(newHealth);

        return newHealth.slider.gameObject;
	}

    //public void RemoveHealthBar(PvPAvatar owner)
    //{
    //    healthTuples.Remove (owner.Attributes.character + owner.ID);
    //}

	public void SetFocus(PvPAvatar target)
	{
        targetAvatar = target;
        //if(target.CompareTag("Player"))
        //    targetAvatar = (PvPAvatar)target.GetComponent<PlayerController>();
        //else if(target.CompareTag("Minion"))
        //    targetAvatar = (PvPAvatar)target.GetComponent<MinionController>();
        //else if(target.CompareTag("Building"))
        //    targetAvatar = (PvPAvatar)target.GetComponent<Building>();
	}

    public void ClearFocus()
    {
        targetAvatar = null;
    }
}

public class HealthTuple {
	public Slider slider;
	public PvPAvatar avatar;
}