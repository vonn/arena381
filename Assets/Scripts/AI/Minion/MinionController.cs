﻿using UnityEngine;
using System.Collections;

public enum MinionType { MELEE, RANGED, TANK, SUPER };

public class MinionController : PvPAvatar
{
    // WHAT AM I ULTIMATELY TRYING TO KILL
    public Building targetStructure;
    protected MinionType _minType { get; set; }
    bool goingToTarget = false;

    protected virtual void Awake()
    {
        base.Awake();
        _type = AvatarType.MINION;
    }

    protected virtual void Update()
    {
        //if (Network.isClient || _state == AvatarState.DEAD) return;

        // No current target
        if (currentTarget == null)
        {
            // my target is still alive!
            if (targetStructure != null && targetStructure._state != AvatarState.DEAD && !goingToTarget)
            {
                // waddle to it, while looking for fights
                GoTowardsTarget();
            }
        }
        else
        {
            goingToTarget = false;
        }
    }

    public void GoTowardsTarget()
    {
        if (_state == AvatarState.DEAD) return;

        SetDestination(targetStructure._trans.position);
        //networkView.RPC("SetDestination", RPCMode.Others, new object[] { targetStructure._trans.position });
        // make sure i'm lookin for a fight~~
        lookForFight = true;
        goingToTarget = true;
    }

    /// <summary>
    /// Set target by ID from pvpav lookup
    /// </summary>
    /// <param name="ID"></param>
    public void SetTargetStructure(int ID)
    {
        targetStructure = PvPAvatar.GetByID(ID).GetComponent<Building>();
        if (targetStructure == null)
        {
            print("Error: target structure ID is not valid");
        }
    }

    /// <summary>
    /// Set target by object
    /// </summary>
    /// <param name="building"></param>
    public void SetTargetStructure(Building building)
    {
        targetStructure = building;
    }
}
