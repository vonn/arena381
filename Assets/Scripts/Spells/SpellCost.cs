﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Spell cost type.
/// </summary>
public enum SpellCostType { NONE, MANA, HEALTH }

/// <summary>
/// Information about spell cost
/// </summary>
public class SpellCost : MonoBehaviour {
	Spell thisSpell;
	public SpellCostType Type = SpellCostType.MANA;
	public int Cost;
	public bool Available;
	
	void Awake() {
		thisSpell = GetComponent<Spell>();
	}
	
	void Update() {
		PvPAttributes attr = thisSpell.Caster.Caster.Attributes;
		
		// determine if the cost is available
		switch (Type) {
		
		case SpellCostType.HEALTH:
			Available = attr.currHealth > Cost;
			break;
		case SpellCostType.MANA:
			Available = attr.currMana >= Cost;
			break;
		case SpellCostType.NONE:
			Available = true;
			break;
			
		}
	}
}
