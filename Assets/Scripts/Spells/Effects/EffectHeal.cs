﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Do the damn heal
/// </summary>
public class EffectHeal : SpellEffect {
	/// <summary>
	/// The amoutn of damage to be dealt
	/// </summary>
	/// <value>The value.</value>
	public int Value;
	
	#region SpellEffect implementation
	public override void Effect()
	{
		// the target
		PvPAvatar tar = GetComponent<SpellTarget>().Target;
		// this spell
		Spell thisSpell = GetComponent<Spell>();

        //tar.TakeDamageSpell(-Value, thisSpell);
        tar.TakeDamage(-Value, (int)DamageType.SPELL, -1);
		// TODO visual fx
	}
	#endregion
}
