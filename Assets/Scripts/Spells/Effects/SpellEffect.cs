﻿using UnityEngine;
using System.Collections;

public class SpellEffect : MonoBehaviour {
	/// <summary>
	/// Effect of this spell
	/// </summary>
	virtual public void Effect() {}
}
