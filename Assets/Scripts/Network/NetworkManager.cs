﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour
{

    #region Static vars
    const int PORT = 25000;
    const int MAXCONNECTIONS = 4;
    const string GAMENAMETYPE = "CSE381_ARENA_CHAMPS_B&K";
    const int MAX_PLAYERS = 2;
    static string currentGameName;
    public static bool readyToStart;

    // SINGLETON
    private static NetworkManager _instance;
    public static NetworkManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<NetworkManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    public static HostData[] hostList;

    public static Dictionary<string, MPPlayer> players = new Dictionary<string, MPPlayer>();
    public MPPlayer hostPlayer; // who's hosting this game?
    public MPPlayer opponentPlayer; // HACK-- HARDCODED PLAYER FOR 2p --HACK
    public MPPlayer me; // KINDA HACK? my current MPPlayer (either host or oppoenent rn)
    public MPPlayer enemy; // KINDA HACK? my current MPPlayer (either host or oppoenent rn)
    #endregion

    #region Member vars
    // TEMPORARY PREFABS FOR NET TESTING
    public GameObject playerPrefab;
    public Transform spawnPos;

    int playerCount = 0;
    static bool serverRegistered;
    #endregion

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
            {
                print("Deleting non-instance NetMgr");
                Destroy(this.gameObject);
                return;
            }
        }

        hostPlayer = transform.FindChild("Host").GetComponent<MPPlayer>();
        opponentPlayer = transform.FindChild("Opponent").GetComponent<MPPlayer>();
    }

    public static void RefreshHostList()
    {
        MasterServer.RequestHostList(GAMENAMETYPE);
    }

    public static void StartServer(string gameName = "Test Game")
    {
        //print("Starting server");
        currentGameName = gameName;
        Network.InitializeServer(MAXCONNECTIONS, PORT, !Network.HavePublicAddress());
        MasterServer.RegisterHost(GAMENAMETYPE, currentGameName);

        // if they finna host a game
        //if (MainMenuController.CurrentMenuState == MenuState.HOST) {
        //print(MainMenuController.CurrentMenuState);
        //    MainMenuController.RequestMenuChange(MenuState.LOBBY);
        //}
    }

    public static void JoinServer(HostData hostData)
    {
        Network.Connect(hostData);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F11))
        {
            print("DO I HAVE A HOST? " + (instance.hostPlayer != null));
        }
    }

    #region Server Events
    /// <summary>
    /// Raised when we initialize host (We are server)
    /// </summary>
    void OnServerInitialized()
    {
        //print ("Server initialized!");
        // Spawns a player for the local host of the game
        //SpawnPlayer();

        // if we're at the start-host screen
        /*if (MainMenuController.CurrentMenuState == MenuState.HOST) {
            HandleHostStarted();
        }*/
    }

    /// <summary>
    /// Raised when we connect to a host (We are client)
    /// </summary>
    void OnConnectedToServer()
    {
        //print ("Connected to server!");

        // send to lobby
        MainMenuController.RequestMenuChange(MenuState.LOBBY);

        // spawn a player for the newly connected client
        //SpawnPlayer();
    }

    /// <summary>
    /// Raises the player connected event.
    /// </summary>
    /// <param name="player">Player.</param>
    void OnPlayerConnected(NetworkPlayer player)
    {
        // make a playerinfo of it and add to dict
        MPPlayer mpp = new MPPlayer(player);
        print("NetMan ~~ New player connected: " + mpp.ID);
        // this is normal way2do
        players.Add(mpp.ID, mpp);
        playerCount++;

        // set up host
        if (Network.player == player)
        {
            print("THIS IS ME");
            hostPlayer.Setup(player);
            me = hostPlayer;
            enemy = opponentPlayer;
        }
        else if (!opponentPlayer.isSetup)
        {
            print("THIS IS NOT ME");
            opponentPlayer.Setup(player);
        }
        else
        {
            print("OPPONENT ALREADY EXISTS");
            return;
        }

        // normal way
        //if (playerCount == MAX_PLAYERS) {
        //    print ("Max Players. Unregistering.");
        //    MasterServer.UnregisterHost();
        //    readyToStart = true;
        //    LobbyController.OnReadyToStart();
        //}

        // hack
        if (opponentPlayer.isSetup)
        {
            print("Max Players. Unregistering.");
            MasterServer.UnregisterHost();
            readyToStart = true;
            //LobbyController.OnReadyToStart();
            OnReadyToStart(false); // call on host
            networkView.RPC("OnReadyToStart", opponentPlayer.NPlayer, new object[] {true}); // send to opponent
        }
    }

    [RPC]
    // deferrence to LobbyCtrl's
    void OnReadyToStart(bool isClient)
    {
        if (isClient)
        {
            print("I AM THE CLIENT");
            me = opponentPlayer;
            enemy = hostPlayer;
        }
        else
        {
            print("I AM THE HOST");
            me = hostPlayer;
            enemy = opponentPlayer;
        }

        LobbyController.OnReadyToStart();
    }

    /// <summary>
    /// Raises the player disconnected event.
    /// </summary>
    /// <param name="player">Player.</param>
    void OnPlayerDisconnected(NetworkPlayer player)
    {
        print("NetMan ~~ Player " + player.ToString() + " disconnected! Cleaning up.");

        // might wanna keep these?
        Network.RemoveRPCs(player);
        Network.DestroyPlayerObjects(player);

        // remove from our dict
        players.Remove(player.ToString());
        playerCount--;

        if (playerCount < MAX_PLAYERS)
        {
            print("< max players. Registering.");
            MasterServer.RegisterHost(GAMENAMETYPE, currentGameName);
            readyToStart = false;
            LobbyController.OnNotReadyToStart();
        }
    }

    /// <summary>
    /// Raises the master server event event.
    /// </summary>
    /// <param name="mse">Mse.</param>
    void OnMasterServerEvent(MasterServerEvent mse)
    {
        switch (mse)
        {
            case MasterServerEvent.RegistrationSucceeded:
                if (!serverRegistered)
                {
                    serverRegistered = true;
                    //print ("Server registered successfully");

                    // Handle that we've started only if we've registered with master server
                    HandleHostStarted();
                }

                break;
            case MasterServerEvent.HostListReceived:
                hostList = MasterServer.PollHostList();
                break;
            default:
                print("Bonk: " + mse);
                break;
        }
    }
    #endregion

    void SpawnPlayer()
    {
        // TODO more logic to place on team, etc.
        GameObject playerObj = (GameObject)Network.Instantiate(playerPrefab, spawnPos.position, Quaternion.identity, 0);
        PlayerController pc = playerObj.GetComponent<PlayerController>();

        //TeamController.SetTeam((int)Team.ONE, pc);

        // setup my uicontroller
        //GameController.M_instance.ui.player = pc;
        //GameController.M_instance.player = pc;

        GameController.instance.Players.Add(pc);
    }

    void HandleHostStarted()
    {
        // make a player for myself (the host/server)
        OnPlayerConnected(Network.player);

        // go to lobby
        //Application.LoadLevel(Scenes.Lobby);
        MainMenuController.RequestMenuChange(MenuState.LOBBY);
    }

    public static void RequestDisconnect(Action OnDisconnect = null)
    {
        Network.Disconnect();

        if (OnDisconnect != null) OnDisconnect();
    }


    public void LoadGame()
    {
        // finally load the next level
        Application.LoadLevel(Scenes.Arena);
    }

    [RPC]
    // deref to chat's
    public void ChatMessage(string message, bool mySend)
    {
        Chat.instance.ChatMessage(message, mySend);
    }

    /// <summary>
    /// Tells player to move
    /// </summary>
    /// <param name="pc"></param>
    /// <param name="name"></param>
    /// <param name="dest"></param>
    public static void PlayerMove(PlayerController pc, string name, Vector3 dest)
    {
        if (pc == null) return;

        if (Network.isServer)
        {
            pc.DropTarget(false);
            pc.SetDestination(dest);
        }
        else
        {
            // can alternatively send this to rpcmode.others
            //pc.networkView.RPC("DropTarget", NetworkManager.instance.enemy.NPlayer, new object[] { false });
            //pc.networkView.RPC("SetDestination", NetworkManager.instance.enemy.NPlayer, new object[] { dest });
            pc.networkView.RPC("DropTarget", RPCMode.All, new object[] { false });
            pc.networkView.RPC("SetDestination", RPCMode.All, new object[] { dest });
        }
    }

    /// <summary>
    /// Gives damage to a player
    /// </summary>
    /// <param name="src"></param>
    /// <param name="target"></param>
    /// <param name="dmgAmount"></param>
    public static void GiveDamage(PvPAvatar src, PvPAvatar target, int dmgAmount)
    {
        // TODO let target know who attacked it
        target.networkView.RPC("TakeDamage", RPCMode.All, new object[] { dmgAmount });
    }

    /// <summary>
    /// Assigns this NetworkViewID to the Player 2. This lets the client that owns nvID to own Player 2.
    /// </summary>
    /// <param name="nvID"></param>
    [RPC]
    public void AssignPlayer2NV(NetworkViewID nvID)
    {
        GameController.instance.pc2.networkView.viewID = nvID;
    }
}
