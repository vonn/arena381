﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A representation of a networked player with relevant game information
/// </summary>
[System.Serializable]
public class MPPlayer : MonoBehaviour // might need to make MonoBehaviour again to attach to a player object
{
    #region MEMBER VARS
    /// <summary>
    /// Unique ID to this player
    /// </summary>
    /// <value>The identifier.</value>
    public string ID;

    public string PlayerName;

    public bool isSetup = false;

    NetworkPlayer _np;
    /// <summary>
    /// Gets or sets the NetworkPlayer associated with this player.
    /// </summary>
    /// <value>The N player.</value>
    public NetworkPlayer NPlayer
    {
        get { return _np; }
        set
        {
            _np = value;
            ID = value.ToString();
        }
    }
    #endregion

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="np">Np.</param>
    public MPPlayer(NetworkPlayer np)
    {
        NPlayer = np;
    }

    public void Setup(NetworkPlayer np)
    {
        NPlayer = np;
        isSetup = true;
    }

    public void Clear()
    {
        ID = null;
        isSetup = false;
    }
}
