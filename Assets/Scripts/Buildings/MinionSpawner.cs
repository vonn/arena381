﻿using UnityEngine;
using System.Collections;

public class MinionSpawner : Building
{
    #region Member Fields
    bool isActive;
    /// <summary>
    /// Whether the spawner is running (basically, while game is running)
    /// </summary>
    public bool Active
    {
        get { return isActive; }
        set { isActive = value; }
    }

    // am i currently spawning minions?
    bool isSpawning;

    // during spawning to spawn minions (they spawn sequentially)
    //float interspawnCD = .7f;
    //float interspawnTime;

    // between spawning cycles
    float spawnerCD = 40;
    float nextSpawnTime;

    // Minions to spawn
    [SerializeField]
    MinionController meleeMinion;
    [SerializeField]
    MinionController rangeMinion;
    //[SerializeField] MinionController tankMinion;
    //[SerializeField] MinionController superMinion;

    // what's the target? (enemy's base)
    public Building minionDestination;

    //bool _spawnSupers;
    //public bool SpawnSuperMinions {
    //    get { return _spawnSupers; }
    //    set { _spawnSupers = value; }
    //}

    //const int wavesTilTank = 4;
    int waveNum;

    const int numMelee = 3;
    const int numRanged = 3;
    //const int numTank = 1;

    public Vector3 spawnPosMelee1, spawnPosMelee2, spawnPosMelee3;
    public Vector3 spawnPosRanged1, spawnPosRanged2, spawnPosRanged3;
    #endregion

    void Awake()
    {
        base.Awake();
        // pool party
        //ObjectPool.CreatePool(meleeMinion, 20);
        //ObjectPool.CreatePool(rangeMinion, 20);
        //ObjectPool.CreatePool(tankMinion, 10);
        //ObjectPool.CreatePool(superMinion, 10);

        spawnPosMelee2 = transform.FindChild("SpawnPosMelee").position;
        spawnPosMelee1 = spawnPosMelee2;
        spawnPosMelee1.z += 1;
        spawnPosMelee3 = spawnPosMelee2;
        spawnPosMelee3.z -= 1;

        spawnPosRanged2 = transform.FindChild("SpawnPosRanged").position;
        spawnPosRanged1 = spawnPosRanged2;
        spawnPosRanged1.z += 1;
        spawnPosRanged3 = spawnPosRanged2;
        spawnPosRanged3.z -= 1;

        nextSpawnTime = Time.time + spawnerCD * .5f; ;
        isActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        // only let the server spawn minions
        if (Network.isClient || _state == AvatarState.DEAD) return;

        // is it time to spawn minions?
        if (isActive && !isSpawning && Time.time >= nextSpawnTime)
        {
            //print("Spawning minions");
            //networkView.RPC("SpawnMinions", RPCMode.All, null);
            SpawnMinions();

            // set cooldown
            nextSpawnTime = Time.time + spawnerCD;
        }
    }

    public void SpawnMinions()
    {
        if (!networkView.isMine) return;

        isSpawning = true;
        waveNum++;
        //bool spawnTank = waveNum % wavesTilTank == 0;

        // spawn 3 melee minions and 3 ranged minions
        MinionController[] minions = new MinionController[6];
        //minions[0] = ObjectPool.Spawn(meleeMinion, spawnPosMelee1);
        //minions[1] = ObjectPool.Spawn(meleeMinion, spawnPosMelee2);
        //minions[2] = ObjectPool.Spawn(meleeMinion, spawnPosMelee3);
        //minions[3] = ObjectPool.Spawn(rangeMinion, spawnPosRanged1);
        //minions[4] = ObjectPool.Spawn(rangeMinion, spawnPosRanged2);
        //minions[5] = ObjectPool.Spawn(rangeMinion, spawnPosRanged3);

        Vector3 facing = _team == (int)Team.ONE ? Vector3.right : Vector3.left;

        minions[0] = (MinionController)Network.Instantiate(meleeMinion, spawnPosMelee1, Quaternion.LookRotation(facing), 0);
        minions[1] = (MinionController)Network.Instantiate(meleeMinion, spawnPosMelee2, Quaternion.LookRotation(facing), 0);
        minions[2] = (MinionController)Network.Instantiate(meleeMinion, spawnPosMelee3, Quaternion.LookRotation(facing), 0);
        minions[3] = (MinionController)Network.Instantiate(rangeMinion, spawnPosRanged1, Quaternion.LookRotation(facing), 0);
        minions[4] = (MinionController)Network.Instantiate(rangeMinion, spawnPosRanged2, Quaternion.LookRotation(facing), 0);
        minions[5] = (MinionController)Network.Instantiate(rangeMinion, spawnPosRanged3, Quaternion.LookRotation(facing), 0);

        for (int i = 0; i < minions.Length; i++)
        {
            minions[i].Respawn();
            minions[i].transform.SetParent(transform.parent);
            minions[i].SetTeam((int)_team);
            minions[i].SetTargetStructure(minionDestination);
        }

        isSpawning = false;
    }

    public void Die(int killerAvID)
    {
        base.Die(killerAvID);

        //print("MAIN STRUCTURE DESTROYED");

        //// EXCEPT WHEN I DIE, THE OTHER TEAM WINS THE GAME!
        //int winningTeam = _team == Team.ONE ? (int)Team.TWO : (int)Team.ONE;
        //GameController.instance.GameOver(winningTeam);
    }
}
