﻿using UnityEngine;
using System.Collections;

public class Turret : Building
{
    LineRenderer _lr;
    //float attackTime;
    Transform shotOrigin;
    //public float attackCD = 1.7f;

    void Awake()
    {
        base.Awake();
        _type = AvatarType.TURRET;
        _lr = GetComponent<LineRenderer>();
        shotOrigin = _trans.FindChild("ShotOrigin");
        isPassive = false;
    }

    // Update is called once per frame
    void Update()
    {
        // do I have a target?
        if (currentTarget != null && _state != AvatarState.DEAD)
        {
            // attack target
            _lr.enabled = true;
            DrawLineToTarget();

            // don't let client execute attack code
            if (!Network.isClient)
            {
                // ATTACK TARGET
                if (nextAtkTime <= Time.time)
                {
                    AttackTarget();
                    nextAtkTime = Time.time + _attr.atkSpd;
                }

                // is my target dead, or out of range?
                if (currentTarget._state == AvatarState.DEAD || !IsTargetInAtkRange())
                {
                    //DropTarget(true);
                    networkView.RPC("DropTarget", RPCMode.All, new object[] { true });
                }
            }
        }
        else
        {
            nextAtkTime = Time.time + _attr.atkSpd;
            _lr.enabled = false;
        }
    }

    void AttackTarget()
    {
        //Destroy(currentTarget.gameObject);

        // hit the target for as much as my atkpower
        GiveDamage(currentTarget, _attr.atkPwr, DamageType.PHYSICAL);
    }

    /// <summary>
    /// Draws the line to target.
    /// </summary>
    void DrawLineToTarget()
    {
        _lr.SetPosition(0, shotOrigin.position);
        _lr.SetPosition(1, currentTarget.transform.position);
    }

    void Die(int killerID)
    {
        base.Die(killerID);
        print("PINK");
    }
}
