﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A structural building in the world
/// </summary>
public class Building : PvPAvatar {

    protected virtual void Awake() {
        base.Awake();
        isPassive = true;
        _type = AvatarType.BUILDING;
    }

    protected void Die(int killerID)
    {
        base.Die(killerID);
        print("PANK");

        //networkView.RPC("DestroyHealthBar", RPCMode.All, null);
        //_trans.renderer.material.SetColor("_Color", Color.grey);
    }
}
