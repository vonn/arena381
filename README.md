# MEOWBA #
A MOBA-like game involving CATS. Currently in *very early* pre-alpha development. Originated as a course project for CSE381.

***

## TO DO ##

### Refactoring ###
* Use constant strings table for RPC's, finding children, etc.
* Abstract the server/client users, as to allow larger number of players to participate in a game (unhack what makes it strictly 2p)

### UI/UX ###
* Visual feedback on prototype art (flashing on attack/dmg, etc.)
* Re-do the Lobby/Chat system, utilizing Unity 4.6 GUI features.
* Tweak Input control system to oversee the user's input
* Reorganize menus to make more cohesive

### Game Logic ###
* Figure out some mechanic to make this game unique and interesting

### Features ###
* Implement some basic spells to the prototype hero
* In-game chat
* Multiplayer games support 2+ players
* Tutorial 

### Networking ###
* Redo client sync to avoid client-side jittering
* Ensure that host is unregistered from the masterserver appropriately (game full)

### Optimizations ###
* Find things to optimize, plz.

### Art/Sound (Assets) ###
* 2D
* * Menu GUI
* * Icons (Heros/avatars)
* 3D
* * Hero 1 (name TBA)
* * Hero 2 (name TBA)
* * Ranged minion
* * Melee minion
* * Tank (strong) minion?
* * Towers
* * Main structure/building (?)
* * Environment/terrain
* * Spell VFX
* * Doodads

***

## Stretch Goals ##
Here is a list of things I'd like to have implemented, but are probably too impractical to undertake currently.

* Re-do networking with Photon (or another Unity networking asset)
* At least 5 playable heroes
* Passable AI (to be used as stand-in's for players)
* Meta-game: Users should have their own account to which stats, levels, unlocked champions, etc. can be tracked.

## Known Bugs ##
* Player doesn't re-target when his target dies.

***

# Changelog #
- [2014-12-18] (And before): Built prototype used for course project, seen here (http://www3.cs.stonybrook.edu/~cse381/projects/Meowba/index.html)